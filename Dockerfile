FROM maven:3.8.3-openjdk-17 As Build
WORKDIR /app
COPY pom.xml .
COPY src ./src
RUN mvn clean package -DskipTest
EXPOSE 8081
ENTRYPOINT [ "java","-jar","target/app.jar" ]
